import React from 'react'
import pngComponents from '../../assets'

const FooterDown = () => {

    const {Png7,Png9,Png10,Png11} = pngComponents;

    return (
        <div className="contener">
                        <div className="sub-container">
                             <div className="footer-icon-container"><a href="#"><img class="icon" src={Png9}
                                        alt="Fblogo"/></a></div>
                            <div className="footer-icon-container"><a href="#"><img class="icon" src={Png10}
                                        alt="Iglogo"/></a></div>
                            <div className="footer-icon-container"><a href="#"><img class="icon" src={Png11}
                                        alt="Linkedinlogo"/></a></div>
                        </div>
                        <div className="sub-container">
                            <div className="JK">
                                <div><img src={Png7} alt='logo'/></div>
                                <div>Jan Kowalski 2020</div>
                            </div>
                        </div>
                    </div>
    )
}

export default FooterDown
