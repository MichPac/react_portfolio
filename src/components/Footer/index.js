import React from 'react'
import FooterDown from '../../elements/FooterEl/FooterDown';
import FooterForm from '../../elements/FooterEl/FooterForm';

function Footer() {

    

    return (
        <footer className="pos-fixed">
            <div className="main-footer_container">
                <div className="main-footer_upper-content">
                    <FooterForm/>
                </div>
                <div className="main-footer_down-content">
                    <FooterDown/>
                </div>
            </div>
        </footer>
    )
}

export default Footer
