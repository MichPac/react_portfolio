import React,{useState,useEffect} from 'react'
import '../../App.css'
import pngComponents from '../../assets/index'
import MainHi from '../../elements/MainEl/MainHi';
import MainPreEl from '../../elements/MainEl/MainPreEl';
import MainSubMenu from '../../elements/MainEl/MainSubMenu';



function Main() {

    const {Png3,Png14,Png16,ProjOnePic,ProjTwoPic,ProjThreePic} = pngComponents;

    const [state, setState] = useState(true)

    useEffect(() => {
       setState(false)
    }, [])

    return (
        <React.Fragment>
        <div className="upper_content">
            <img className="img one" src={Png14} alt="szlaczek"/>
            <img className="img two" src={Png14} alt="szlaczek"/>
            <img className="img three" src={Png16} alt="szlaczek"/>
            <img className="img" src={Png16} alt="szlaczek"/>
            <MainHi/>
            <div className="photo_background">
                <img className="facefoto" src={Png3} alt='FaceFoto'/>
            </div>
          </div>
        
          <div className="content_center-wrapper">
        <div className="center_content">
        <div id="table" className={ state ? "content_center-backgroundImg" : "content_center-backgroundImgAnimate" }>
            <div class="upper_bar">
                            <div class="dots">
                                <div class="dot-red"></div>
                                <div class="dot-yellow"></div>
                                <div class="dot-green"></div>
                            </div>
                            <p>//About</p>
                        </div>
                        <div class="content_center-backgroundImg_downTxt">
                            <div class="About">
                                <p>//<span> About</span></p>
                                <div class="contnet-wrapper">
                                    <div class="leftSide">
                                        <p>&lt;/ <span>Me </span>&gt;</p>
                                        <div class="pre">
                                            <MainPreEl/>
                                        </div>
                                    </div>
                                    <div class="rightSide">
                                        <p>&lt; / <span>Skills </span>&gt;</p>
                                        <div class="Skills">
                                            <div class="flex-content__wrapper">
                                                <div class="flex-product__wrapper">
                                                    <div class="Skills-bar css">
                                                        <p>CSS</p>
                                                    </div>
                                                </div>
                                                <div class="flex-product__wrapper">
                                                    <p>80%</p>
                                                </div>
                                            </div>

                                            <div class="flex-content__wrapper">
                                                <div class="flex-product__wrapper">
                                                    <div class="Skills-bar html">
                                                        <p>HTML</p>
                                                    </div>
                                                </div>
                                                <div class="flex-product__wrapper">
                                                    <p>90%</p>
                                                </div>
                                            </div>

                                            <div class="flex-content__wrapper">
                                                <div class="flex-product__wrapper">
                                                    <div class="Skills-bar css">
                                                        <p>JAVASCRIPT</p>
                                                    </div>
                                                </div>
                                                <div class="flex-product__wrapper">
                                                    <p>80%</p>
                                                </div>
                                            </div>

                                            <div class="flex-content__wrapper">
                                                <div class="flex-product__wrapper">
                                                    <div class="Skills-bar html">
                                                        <p>HTML</p>
                                                    </div>
                                                </div>
                                                <div class="flex-product__wrapper">
                                                    <p>90%</p>
                                                </div>
                                            </div>

                                            <div class="flex-content__wrapper">
                                                <div class="flex-product__wrapper">
                                                    <div class="Skills-bar css">
                                                        <p>REACT</p>
                                                    </div>
                                                </div>
                                                <div class="flex-product__wrapper">
                                                    <p>80%</p>
                                                </div>
                                            </div>

                                            <div class="flex-content__wrapper">
                                                <div class="flex-product__wrapper">
                                                    <div class="Skills-bar js">
                                                        <p>JS</p>
                                                    </div>
                                                </div>
                                                <div class="flex-product__wrapper">
                                                    <p>60%</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
        </div>

        <div className="down_content">
            <MainSubMenu/>
            <div className="projects_container">
                <div className="projects_wrapper">
                    <div className="projects-pictures"><img src={ProjOnePic} alt="proejkt1"/></div>
                    <div className="projects-pictures"><img src={ProjTwoPic} alt="projekt2"/></div>
                    <div className="projects-pictures"><img src={ProjThreePic} alt="proejkt3"/></div>
                </div>
                <div className="projects_wrapper column2">
                    <div className="projects-pictures"><img src={ProjTwoPic} alt="proejkt4"/></div>
                </div>
                <div className="line">
                    <div className="Contact">
                    <h2><span>//</span>Contact</h2>
                    </div>
                </div>
            </div>
        </div>
        </React.Fragment>
    )
}

export default Main
