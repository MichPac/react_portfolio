import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

const components = {
    Header,Main,Footer
}

export default components