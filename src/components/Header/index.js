import React from 'react'
// import '../../App.css'
import pngComponents from '../../assets/index'

function Header() {

  const {Png7,Png9,Png10,Png11} = pngComponents;

    return (
        <React.Fragment>
             <header>
              <img className="logo" src={Png7} alt='logo'/>
                  <nav>
                    <ul className="nav_links nav_linksH">
                        <li><a href="#">Projects</a></li>
                        <li><a href="#">About me</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                  </nav>
                  <div className="nav_logo">
                <a href="#"><img className="icon" src={Png9} alt="Fblogo"/></a>
                <a href="#"><img className="icon"  src={Png10} alt="Iglogo"/></a>
                <a href="#"><img className="icon"  src={Png11} alt="Linkedinlogo"/></a>
            </div>
          </header>
        </React.Fragment>
    )
}

export default Header
