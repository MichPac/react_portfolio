import React from 'react';
// import '../../App.css'
import components from '../../components'

const Homepage = () => {

    const {Header,Main,Footer} = components;

    return (
      
      <div>
        <div className="body-first-child">
          <Header/>
          <Main/>
        </div>

        <div class="body-second-child">
          <Footer/>
        </div>
      </div>
    );
  }

export default Homepage;


